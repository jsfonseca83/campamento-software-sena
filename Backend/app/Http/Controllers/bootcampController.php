<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Bootcamps;
use App\Http\Requests\StoreBootcampRequest;
//use Illuminate\Support\Facades\Validator;

class bootcampController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //Metodo json : transmite responde en formato json
        //Parametros: 
        //1. Datos a transmitir
        //2. Codigo http de la respuesta
        return response()->json(["success" => true,
                                 "data" => Bootcamps::all()]
                                ,200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreBootcampRequest $request)
    {
        //Crear el nuevo bootcamp 
        /*$newBootcamp = new Bootcamps;
        $newBootcamp->name = $request->name;
        $newBootcamp->website = $request->website;
        $newBootcamp->phone = $request->phone;
        $newBootcamp->description = $request->description;
        $newBootcamp->user_id = $request->user_id;
        $newBootcamp->average_rating = $request->average_rating;
        $newBootcamp->average_cost = $request->average_cost;
        $newBootcamp->save();*/
        //1. Reglas de validacion
        /*$reglas = [
            "name" => 'required'
        ];
        //2. Crear el objeto validador
        $v = Validator::make( $request->all() , $reglas);

        //3. Validar
        if($v->fails()){
            //4. Si la validacion falla
            return response()->json( [
                "success" => false,
                "error" => $v->errors()
            ], 404 
        );
        }
        */
        return response()->json(["success" => true,
                                 "data" => Bootcamps::create($request->all())]
                                ,201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return response()->json(["success" => true,
                                 "data" => Bootcamps::find($id)]
                                ,200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        echo "aqui se va a actializar un producto cuyo id es:$id";
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $b = Bootcamps::find($id);
        $b->update($request->all());
        return $b;

        return response()->json(["success" => true,
                                 "data" => $b      ]
                                ,200);
        return $b;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)    
    {
        $b = Bootcamps::find($id);
        $b->delete();
        

        return response()->json(["success" => true,
                                 "data" =>  $b   ]
                                ,200);
                                return $b;
    }
}
