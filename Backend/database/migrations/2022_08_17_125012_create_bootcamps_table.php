<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bootcamps', function (Blueprint $table) {
            $table->id();
            $table->string('name',100)->nulleable;
            $table->string('description',150)->nulleable;
            $table->string('website',100)->nulleable;
            $table->string('phone',100)->nulleable;
            $table->float('average_rating',100)->nulleable;
            $table->decimal('average_cost',65)->nulleable;
            $table->foreignId('user_id')->constrained();
            $table->timestamps();

        });
    }       

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bootcamps');
    }
};
