<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Bootcamps;
use File;

class BootcampSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //1. Truncar la tabla bootcamps
        //Bootcamps::truncate();
        //2. Leer el archivo bootcamps.json
        $json = File::get("database/_data/bootcamps.json");
        //2.1 Convertir el contenido json
        $arreglo_bootcamps = json_decode($json);
        //3. Recorrer el archivo y por cada bootcamp
        foreach($arreglo_bootcamps as $bootcamps){
            //4. Crear un bootcamp por cada uno
            $b = new Bootcamps();
            $b->name = $bootcamps->name;
            $b->website = $bootcamps->website;
            $b->phone = $bootcamps->phone;
            $b->description = $bootcamps->description;
            $b->user_id = 1;
            $b->average_rating = $bootcamps->average_rating;
            $b->average_cost = $bootcamps->average_cost;
            $b->save();
        }        
    }
}
